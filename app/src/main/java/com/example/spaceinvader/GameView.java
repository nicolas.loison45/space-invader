package com.example.spaceinvader;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Build;
import android.text.TextPaint;
import android.util.Log;
import android.view.DragAndDropPermissions;
import android.view.MotionEvent;
import android.view.SurfaceView;
import android.view.animation.BounceInterpolator;

import com.example.spaceinvader.Background;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;


import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class GameView extends SurfaceView implements Runnable{

    public static final double MAX_UPS = 50.0;
    private static final double UPS_PERIOD = 1E+3/MAX_UPS;

    private Thread thread;
    private boolean isPlaying;
    private Background background1,background2, death;
    private int score = 0, scoreWidth, xHighscore;
    private List<Integer> listeScores;
    static int screenX, screenY;
    private SharedPreferences prefs;
    private Paint paint, paintSpeed;
    private Random random;
    static float screenRatioX, screenRatioY;
    private Vaisseau vaisseau;
    private List<Bullet> bullets;
    private List<Bonus> bonuses;
    private Mechant[] mechants;
    private boolean isGameOver = false;
    private GameActivity activity;
    private Point playerPoint;
    private List<Explosion> explosions;
    private SoundPool soundPool;
    private int soundBullet, soundShieldup, soundShieldDown, soundExplosion, soundSpeedup;

    static int nbTours = 0;
    int updateCount = 0;
    int frameCount = 0;
    long startTime;
    long elapsedTime;
    long sleepTime;
    private double averageUPS;
    private double averageFPS;

    private OrientationData orientationData;
    private boolean isGyro;

    public GameView(GameActivity activity, int screenX, int screenY) {
        super(activity);

        this.activity = activity;

        prefs = activity.getSharedPreferences("game", Context.MODE_PRIVATE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            AudioAttributes audioAttributes = new AudioAttributes.Builder()
                    .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
                    .setUsage(AudioAttributes.USAGE_GAME)
                    .build();

            soundPool = new SoundPool.Builder().setMaxStreams(10).setAudioAttributes(audioAttributes).build();

        } else
            soundPool = new SoundPool(1, AudioManager.STREAM_MUSIC, 0);

        soundBullet = soundPool.load(activity, R.raw.laser9, 1);
        soundExplosion = soundPool.load(activity, R.raw.explosion, 0);

        this.screenX = screenX; // 1080
        this.screenY = screenY; // 2034

        screenRatioX = 1080f / screenX;
        screenRatioY = 2034f / screenY;

        background1 = new Background(screenX, screenY, getResources());
        background2 = new Background(screenX, screenY, getResources());
        death = new Background(screenX, screenY, getResources());

        vaisseau = new Vaisseau(this, screenX, getResources());
        playerPoint = new Point(vaisseau.x, vaisseau.y);

        background2.y = -screenY;

        bullets = new ArrayList<>();
        bonuses = new ArrayList<>();
        explosions = new ArrayList<>();

        Gson gson = new Gson();
        String json = prefs.getString("listearcadescores", null);
        Type type = new TypeToken<ArrayList<Integer>>() {}.getType();
        listeScores = gson.fromJson(json, type);

        if (listeScores == null) {
            listeScores = new ArrayList<>();
        }

        mechants = new Mechant[4];
        for (int i = 0; i<4; i++) {

            Mechant mechant = new Mechant(getResources());
            mechants[i] = mechant;

        }

        paint = new Paint();
        paint.setColor(Color.WHITE);
        paint.setTextSize(128);

        paintSpeed = new Paint();
        paintSpeed.setColor(Color.WHITE);
        paintSpeed.setTextSize(64);

        random = new Random();

        orientationData = new OrientationData(activity);
        orientationData.register();
        isGyro = prefs.getBoolean("isGyro", true);
        if (isGyro) vaisseau.toShoot++;
    }


    @Override
    public void run() {

        while (isPlaying) {

            update();
            draw();
            sleep();
            fps();
            nbTours++;

        }

    }

    private void fps() {
        elapsedTime = System.currentTimeMillis() - startTime;
        if(elapsedTime >= 1000) {
            averageUPS = updateCount / (1E-3 * elapsedTime);
            averageFPS = frameCount / (1E-3 * elapsedTime);
            updateCount = 0;
            frameCount = 0;
            startTime = System.currentTimeMillis();
        }
    }

    private void update() {

        background1.update();
        background2.update();

        if (isGyro){
            vaisseau.x -= orientationData.getAccelOutput()[0] * Math.abs(orientationData.getAccelOutput()[0]);
            vaisseau.y += orientationData.getAccelOutput()[1] * Math.abs(orientationData.getAccelOutput()[1]);
            vaisseau.updateGyro();
        }
        else {
            vaisseau.update(playerPoint);
        }

        updateBullet();
        updateBonus();
        updateMechant();
        updateExplosion();

        updateCount++;

    }

    private void updateExplosion() {
        List<Explosion> trash = new ArrayList<>();

        for(Explosion explosion : explosions) {
            if (explosion.fini){
                trash.add(explosion);
            }
        }

        for (Explosion explosion : trash){
            explosions.remove(explosion);
        }
    }

    private void updateBonus() {

        List<Bonus> trash = new ArrayList<>();
        for (Bonus bonus : bonuses) {

            if (bonus.y > screenY) {
                trash.add(bonus);
            }

            if (Rect.intersects(vaisseau.getCollisionShape(), bonus.getCollisionShape())){

                if (bonus.powerup.equals("speedup")){
                    vaisseau.shootSpeed--;
                    if (vaisseau.shootSpeed < 3) {
                        vaisseau.shootSpeed=3;
                    }
                    else {
                        if (!prefs.getBoolean("isMute", false))
                            soundPool.play(soundSpeedup, 1, 1, 0, 0, 1);
                    }
                }
                if (bonus.powerup.equals("shield")) {
                    vaisseau.isShield();
                    if (!prefs.getBoolean("isMute", false))
                        soundPool.play(soundShieldup, 1, 1, 0, 0, 1);
                }

                trash.add(bonus);

            }

            bonus.y += 15 * screenRatioY;

        }

        for (Bonus bonus : trash) {
            bonuses.remove(bonus);
        }
    }

    private void updateBullet() {

        List<Bullet> trash = new ArrayList<>();

        for (Bullet bullet : bullets) {

            if (bullet.y > screenY) {
                trash.add(bullet);
            }

            for (Mechant mechant : mechants) {

                if (Rect.intersects(mechant.getCollisionShape(), bullet.getCollisionShape())){

                    score++;
                    newBonus(mechant);
                    newExplosion(mechant);
                    mechant.y = screenY + 500;
                    bullet.y = screenY + 500;
                    mechant.wasShot = true;

                }

            }

            bullet.y -= 30 * screenRatioY;
        }

        for (Bullet bullet : trash) {
            bullets.remove(bullet);
        }
    }


    private void updateMechant() {

        for (Mechant mechant : mechants) {

            int newX = random.nextInt(screenX - mechant.width);

            mechant.update(newX); // met a jour le rectangle hitbox (pour le test)

            if (Rect.intersects(mechant.getCollisionShape(), vaisseau.getCollisionShape())){

                if (vaisseau.isShielded){
                    mechant.isOut(newX);
                }
                else{
                    isGameOver = true;
                    return;
                }
            }


            scoreWidth = Integer.toString(score).length();
            xHighscore = (screenX - (80 * scoreWidth)) / 2 ;

        }
    }

    private void draw() {

        if (getHolder().getSurface().isValid()) {

            Canvas canvas = getHolder().lockCanvas();

            canvas.drawBitmap(background1.background1, background1.x, background1.y, paint);
            canvas.drawBitmap(background2.background2, background2.x, background2.y, paint);

            for (Bonus bonus : bonuses) {
                if (bonus.powerup.equals("speedup")){
                    canvas.drawBitmap(bonus.speedup, bonus.x, bonus.y, paint);
                }
                if (bonus.powerup.equals("shield")) {
                    canvas.drawBitmap(bonus.shield, bonus.x, bonus.y, paint);
                }
            }

            for (Explosion explosion : explosions) {
                canvas.drawBitmap(explosion.getExplosion(), explosion.x, explosion.y, paint);
            }

            for (Mechant mechant : mechants) {
                //canvas.drawRect(mechant.rectangle, paint); affichage de la hitbox
                canvas.drawBitmap(mechant.getMechant(), mechant.x, mechant.y, paint);
            }

            canvas.drawText(score + "", xHighscore, 164, paint);

            //canvas.drawRect(vaisseau.rectangle, paint);
            if (vaisseau.isShielded){
                canvas.drawBitmap(vaisseau.getShield(), vaisseau.x, vaisseau.y, paint);
            }
            else{
                canvas.drawBitmap(vaisseau.getFlight(), vaisseau.x, vaisseau.y, paint);
            }

            if (isGameOver) {
                isPlaying = false;
                saveIfHighScore();
                saveScore();
                getHolder().unlockCanvasAndPost(canvas);
                endgame();
                return;
            }

            for (Bullet bullet : bullets) {
                canvas.drawBitmap(bullet.bullet, bullet.x, bullet.y, paint);
            }

            int speed =  -(vaisseau.shootSpeed-9);

            if (speed == 6) canvas.drawText("Max Speed", 50, 125, paintSpeed);
            else canvas.drawText("Speed: " +speed, 50, 125, paintSpeed);

            if (isGyro) {
                canvas.drawText("accel x: " + orientationData.getAccelOutput()[0], 100, 300, paintSpeed);
                canvas.drawText("accel y: " + orientationData.getAccelOutput()[1], 100, 400, paintSpeed);
            }

            getHolder().unlockCanvasAndPost(canvas);
            frameCount++;

        }

    }


    private void endgame() {

        try {
            Thread.sleep(3000);
            activity.startActivity(new Intent(activity, PostActivity.class));
            activity.finish();

        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    private void saveScore() {
        SharedPreferences.Editor editor  = prefs.edit();
        editor.putInt("lastscorearcade", score);
        editor.apply();
    }

    private void saveIfHighScore() {
        if (listeScores.size() < 10) {
            listeScores.add(score);
            Collections.sort(listeScores);
            System.out.println("ajout du score " + score);
            for (int score : listeScores) {
                System.out.println("score " + score);
            }
        }
        else {
            if (score > listeScores.get(0)) {
                insertScore();
            }
        }
        SharedPreferences.Editor editor  = prefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(listeScores);
        editor.putString("listearcadescores", json);
        editor.apply();
    }

    private void insertScore() {
        int i = listeScores.size()-1;
        while (i >= 0 && listeScores.get(i) >= score) {
            i--;
        }
        listeScores.remove(0);
        listeScores.add(i, score);
    }

    public void sleep() {

        // Pause game loop to not exceed target UPS
        elapsedTime = System.currentTimeMillis() - startTime;
        sleepTime = (long) (updateCount*UPS_PERIOD - elapsedTime);
        if(sleepTime > 0) {
            try {
                Thread.sleep(sleepTime);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }
    public void resume() {
        isPlaying = true;
        thread = new Thread(this);
        thread.start();

    }

    public void pause() {

        try {
            isPlaying = false;
            thread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        switch (event.getAction()) {
            case MotionEvent.ACTION_MOVE:
                if (vaisseau.rectangle.contains((int) event.getX(), (int) event.getY())) {
                    playerPoint.set((int) event.getX(), (int) event.getY());
                    vaisseau.toShoot++;
                    performClick();
                }
                break;
            case MotionEvent.ACTION_DOWN:
                break;
        }
        return true;
    }
    @Override
    public boolean performClick() {
        // Calls the super implementation, which generates an AccessibilityEvent
        // and calls the onClick() listener on the view, if any
        super.performClick();
        // Handle the action for the custom click here
        return true;
    }

    public void newBullet() {

        if (!prefs.getBoolean("isMute", false))
            soundPool.play(soundBullet, 1, 1, 0, 0, 1);

        Bullet bullet = new Bullet(getResources());
        bullet.x =  (vaisseau.x + (vaisseau.width / 2)) - (bullet.width / 2);
        bullet.y = vaisseau.y - (bullet.height);
        bullets.add(bullet);

    }

    private void newBonus(Mechant mechant) {

        int bound = random.nextInt(20);
        if (bound == 0){
            if (vaisseau.shootSpeed != 3) {
                Bonus bonus = new Bonus(getResources(), "speedup");
                bonus.x = mechant.x + (mechant.width / 2);
                bonus.y = mechant.y + (mechant.height / 2);
                soundSpeedup = soundPool.load(activity, R.raw.speedup, 1);
                bonuses.add(bonus);
            }
        }

        int bound2 = random.nextInt(40);
        if (bound2 == 1 && !vaisseau.isShielded){
            Bonus bonus = new Bonus(getResources(), "shield");
            bonus.x = mechant.x + (mechant.width / 2);
            bonus.y = mechant.y + (mechant.height / 2);
            soundShieldup = soundPool.load(activity, R.raw.shieldup, 2);
            soundShieldDown = soundPool.load(activity, R.raw.shielddown, 2);
            bonuses.add(bonus);
        }
    }

    private void newExplosion(Mechant mechant) {

        if (!prefs.getBoolean("isMute", false))
            soundPool.play(soundExplosion, 1, 1, 0, 0, 1);

        Explosion explosion = new Explosion(getResources());
        explosion.x = (mechant.x + (mechant.width / 2)) - explosion.width/2;
        explosion.y = (mechant.y + mechant.height) - explosion.height/2;
        explosions.add(explosion);
    }


    public void shieldDown() {

        if (!prefs.getBoolean("isMute", false))
            soundPool.play(soundShieldDown, 1, 1, 0, 0, 1);

    }
}