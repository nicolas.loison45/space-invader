package com.example.spaceinvader;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;

public class Nuclear {
    private int x;
    private int y;
    private int width;
    private int height;
    private int row, ligne, col;
    private int numFrames;
    private Animatio animation = new Animatio();
    private Bitmap explosion;

    public Nuclear(int x, int y, Resources res)
    {
        this.x = x;
        this.y = y;

        numFrames = 24;
        col = 8;
        ligne = 3;

        Bitmap[] image = new Bitmap[numFrames];
        explosion = BitmapFactory.decodeResource(res, R.drawable.explosionseize);

        width = explosion.getWidth() / col;
        height = explosion.getHeight()/ ligne;

        for(int i = 0; i<image.length; i++)
        {
            if(i%col==0&&i>0)row++;
            image[i] = Bitmap.createBitmap(explosion, (i-(col*row))*width, row*height, width, height);
        }
        animation.setFrames(image);
        animation.setDelay(10);

    }
    public void draw(Canvas canvas)
    {
        if(!animation.playedOnce())
        {
            canvas.drawBitmap(animation.getImage(),x,y,null);
        }

    }
    public void update()
    {
        if(!animation.playedOnce())
        {
            animation.update();
        }
    }
    public int getHeight(){return height;}
}