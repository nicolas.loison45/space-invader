package com.example.spaceinvader;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

public class Background {

    int x = 0, y = 0, width, height, screenX, screenY;
    float screenRatioX, screenRatioY;
    Bitmap background1, background2;

    public Background(int screenX, int screenY, Resources res) {

        this.screenY = screenY;

        background1 = BitmapFactory.decodeResource(res, R.drawable.background2034);
        background2 = BitmapFactory.decodeResource(res, R.drawable.background22034);
//
        screenRatioX = 1080f / screenX;
        screenRatioY = 2034f / screenY;

        background1 = Bitmap.createScaledBitmap(background1, screenX, screenY, false);
        background2 = Bitmap.createScaledBitmap(background2, screenX, screenY, false);

    }

    public void update() {
        y += 4;

        if (y > this.screenY) {
            y = -this.screenY+3;
        }
    }

}
