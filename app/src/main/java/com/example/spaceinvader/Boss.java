package com.example.spaceinvader;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;

import java.util.Random;

import static com.example.spaceinvader.WaveView.nbTours;
import static com.example.spaceinvader.WaveView.screenRatioX;
import static com.example.spaceinvader.WaveView.screenRatioY;
import static com.example.spaceinvader.WaveView.screenX;
import static com.example.spaceinvader.WaveView.screenY;

public class Boss {

    Rect rectangle;
    int x, y, width, height, dmgCounter = 0;
    private Resources resources;
    Bitmap boss, damaged;
    Random random;
    private int hp = 10;
    boolean isHurt;

    Boss (Resources res) {

        this.resources = res;

        boss = BitmapFactory.decodeResource(res, R.drawable.boss);
        damaged = BitmapFactory.decodeResource(res, R.drawable.bossdmg);

        width = boss.getWidth();
        height = boss.getHeight();

        width = (int) (width * screenRatioX);
        height = (int) (height * screenRatioY);

        boss = Bitmap.createScaledBitmap(boss, width, height, false);
        damaged = Bitmap.createScaledBitmap(damaged, width, height, false);

        rectangle = new Rect(x, y, x + width, y + height);
        random = new Random();
    }

    public Bitmap getBoss() {
        if (isHurt) {
            if (dmgCounter == 10) {
                isHurt = false;
                dmgCounter = 0;
            }
            dmgCounter++;
            return damaged;
        }
        return boss;
    }

    public void update() {

        if (nbTours % 100 <50) {
            deplacement(0);
        }
        if (nbTours % 100 >50) {
            deplacement(1);
        }


        if (x + width > screenX) {
            x = screenX - width;
        }
        if (x < 0) {
            x = 0;
        }
        if (y > screenY/2) {
            y = screenY/2;
        }
        if (y < 40) {
            y = 40;
        }

        rectangle.set(x, y, x + width, y + height);

    }

    private void deplacement(int direction) {

        switch (direction) {
            case 0:
                x += 10;
                y-=5;
                break;
            case 1:
                x-=10;
                y+=5;
                break;
        }

    }

    public int getHp() {
        return this.hp;
    }

    public void setHp(int hp) {
        this.hp = hp;
    }

    public Bitmap getMechant() { return boss; }

    public Rect getCollisionShape () {
        return rectangle;
    }

}