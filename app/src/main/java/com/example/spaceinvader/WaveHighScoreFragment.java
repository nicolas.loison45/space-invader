package com.example.spaceinvader;

import android.annotation.SuppressLint;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static android.content.Context.MODE_PRIVATE;

public class WaveHighScoreFragment extends Fragment {
    private static final String TAG = "WaveScores";

    private List<ScoreWave> listeScore;

    @SuppressLint("SetTextI18n")
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.tabwave_fragment, container, false);
        rootView.setTag(TAG);
        final SharedPreferences prefs = Objects.requireNonNull(getActivity()).getSharedPreferences("game", MODE_PRIVATE);

        TextView textlast = rootView.findViewById(R.id.lastscorewave);
        if (prefs.getInt("nbwave", 0) == 0) textlast.setText("");
        else textlast.setText("Last score : " + prefs.getInt("lastscorewave", 0) + " on wave " + prefs.getInt("nbwave", 0));

        TextView textbests = rootView.findViewById(R.id.bestwave);
        WaveBests(textbests, prefs);

        return rootView;
    }

    @SuppressLint("SetTextI18n")
    public void WaveBests(TextView textbests, SharedPreferences prefs) {
        textbests.setText("Pas encore de meilleurs scores");
        textbests.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);

        Gson gson = new Gson();
        String json = prefs.getString("listewavescores", null);
        Type type = new TypeToken<ArrayList<ScoreWave>>() {}.getType();
        listeScore = gson.fromJson(json, type);

        if (listeScore == null) {
            listeScore = new ArrayList<>();
        }
        else {
            textbests.setText("");
            textbests.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_START);

            int cpt = 1;
            String num = "0";
            for (int i = listeScore.size()-1 ; i>=0; i--) {
                if (cpt >= 10) num = "";
                textbests.setText(textbests.getText() + num + cpt + ".     " + listeScore.get(i).toString() + "\n\n");
                cpt++;
            }
        }
    }

}
