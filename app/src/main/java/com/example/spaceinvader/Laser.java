package com.example.spaceinvader;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;

import static com.example.spaceinvader.WaveView.screenRatioX;
import static com.example.spaceinvader.WaveView.screenRatioY;

public class Laser {

    int x, y, width, height, speed;
    Bitmap laser, laserEnnemi;

    Laser (Resources res) {

        laser = BitmapFactory.decodeResource(res,R.drawable.laserblue);
        laserEnnemi = BitmapFactory.decodeResource(res, R.drawable.laserred);

        width = laser.getWidth();
        height = laser.getHeight();

        width *= 1.5;
        height /= 1;

        width = (int) (width * screenRatioX);
        height = (int) (height * screenRatioY);

        laser = Bitmap.createScaledBitmap(laser, width, height, false);
        laserEnnemi = Bitmap.createScaledBitmap(laserEnnemi, width, height, false);

    }

    Rect getCollisionShape () {
        return new Rect(x, y, x + width, y + height);
    }

}