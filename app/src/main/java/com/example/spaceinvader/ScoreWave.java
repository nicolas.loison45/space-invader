package com.example.spaceinvader;

import androidx.annotation.NonNull;

public class ScoreWave implements Comparable<ScoreWave>{

    private int wave, score;

    public ScoreWave() {
        this.wave = 1;
        this.score = 0;
    }


    public int getWave() {
        return wave;
    }

    public void setWave(int wave) {
        this.wave = wave;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }
    public void incr() {
        this.score++;
    }

    public void incrWave() {
        this.wave++;
    }

    @Override
    public int compareTo(ScoreWave scoreWave) {
        return 50*(this.getScore() - scoreWave.getScore()) - (this.getWave() - scoreWave.getWave());
    }

    @NonNull
    @Override
    public String toString() {
        return this.score + " on wave " + this.wave;
    }

}
