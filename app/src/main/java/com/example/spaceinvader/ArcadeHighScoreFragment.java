package com.example.spaceinvader;

import android.annotation.SuppressLint;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static android.content.Context.MODE_PRIVATE;

public class ArcadeHighScoreFragment extends Fragment {

    private static final String TAG = "ArcadeScores";
    private List<Integer> listeScore;

    @SuppressLint("SetTextI18n")
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.tabarcade_fragment, container, false);
        rootView.setTag(TAG);
        final SharedPreferences prefs = Objects.requireNonNull(getActivity()).getSharedPreferences("game", MODE_PRIVATE);

        TextView text = rootView.findViewById(R.id.lastscorearcade);
        if (prefs.getInt("lastscorearcade", 0) == 0) text.setText("");
        else text.setText("Last score : " + prefs.getInt("lastscorearcade", 0));

        TextView textbests = rootView.findViewById(R.id.bestarcade);
        arcadeBests(textbests, prefs);

        return rootView;
    }

    @SuppressLint("SetTextI18n")
    public void arcadeBests(TextView textbests, SharedPreferences prefs) {
        textbests.setText("Pas encore de meilleurs scores");
        textbests.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);

        Gson gson = new Gson();
        String json = prefs.getString("listearcadescores", null);
        Type type = new TypeToken<ArrayList<Integer>>() {}.getType();
        listeScore = gson.fromJson(json, type);

        if (listeScore == null) {
            listeScore = new ArrayList<>();
        }
        else {
            textbests.setText("");
            textbests.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_START);

            int cpt = 1;
            String num = "0";
            for (int i = listeScore.size() - 1; i >= 0; i--) {
                if (cpt >= 10) num = "";
                textbests.setText(textbests.getText() + num + cpt + ".     " + listeScore.get(i) + "\n\n");
                cpt++;
            }
        }
    }
}
