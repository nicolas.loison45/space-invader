package com.example.spaceinvader;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Build;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.view.MotionEvent;
import android.view.SurfaceView;


import androidx.core.content.ContextCompat;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.nio.channels.FileLock;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Random;

import static android.content.Context.VIBRATOR_SERVICE;

public class WaveView extends SurfaceView implements Runnable{

    public static final double MAX_UPS = 50.0;
    private static final double UPS_PERIOD = 1E+3/MAX_UPS;

    private Thread thread;
    private boolean isPlaying;
    private Background background1,background2;
    private Point playerPoint;
    private VaisseauWave vaisseau;
    private Flammes flammes;
    private ScoreWave scoreWave;
    private List<ScoreWave> listeScores;
    static int screenX, screenY;
    private SharedPreferences prefs;
    private Paint paint, paintWave, paintEnnemis, paintBoss, paintTransi;
    private Random random;
    static float screenRatioX, screenRatioY;
    private boolean isGameOver = false, isGameWin=false;
    private WaveActivity activity;
    private List<Ennemi> ennemis;
    private List<Boss> bosses;
    private List<Ennemi> trashEnemis;
    private List<Explosion> explosions; // plu utilisées et remplacées par des Nuclear
    private List<Nuclear> nuclears;     // Nuclear utilise la class Animatio qui est la propre
    private List<Laser> trash, bulletEnnemis;
    private SoundPool soundPool;
    private int soundBullet, soundShieldup, soundShieldDown, soundExplosion, soundSpeedup;

    static int nbTours = 0;
    private boolean bossWave = false;
    private ArrayList<Laser> bulletsBoss;

    int updateCount = 0;
    int frameCount = 0;
    long startTime;
    long elapsedTime;
    long sleepTime;
    private double averageUPS;
    private double averageFPS;

    private OrientationData orientationData;
    private boolean isGyro;

    public WaveView(WaveActivity activity, int screenX, int screenY) {
        super(activity);

        this.activity = activity;
        random = new Random();

        prefs = activity.getSharedPreferences("game", Context.MODE_PRIVATE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            AudioAttributes audioAttributes = new AudioAttributes.Builder()
                    .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
                    .setUsage(AudioAttributes.USAGE_GAME)
                    .build();

            soundPool = new SoundPool.Builder().setMaxStreams(10).setAudioAttributes(audioAttributes).build();

        } else
            soundPool = new SoundPool(1, AudioManager.STREAM_MUSIC, 0);

        soundBullet = soundPool.load(activity, R.raw.laser9, 1);
        soundExplosion = soundPool.load(activity, R.raw.explosion, 0);

        this.screenX = screenX; // 1080
        this.screenY = screenY; // 2034

        screenRatioX = 1080f / screenX;
        screenRatioY = 2034f / screenY;

        background1 = new Background(screenX, screenY, getResources());
        background2 = new Background(screenX, screenY, getResources());
        background2.y = -screenY;

        vaisseau = new VaisseauWave(this, screenX, getResources());
        playerPoint = new Point(vaisseau.x, vaisseau.y);
        flammes = new Flammes(vaisseau.x, vaisseau.y, getResources());

        scoreWave = new ScoreWave();

        orientationData = new OrientationData(activity);
        orientationData.register();
        isGyro = prefs.getBoolean("isGyro", true);

        arrayInit();
        initPaint();

        firstWave();

    }

    public void vibrate(long millis) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            ((Vibrator) Objects.requireNonNull(activity.getSystemService(VIBRATOR_SERVICE)))
                    .vibrate(VibrationEffect.createOneShot(millis, VibrationEffect.DEFAULT_AMPLITUDE));
        }
        else {
            ((Vibrator) Objects.requireNonNull(activity.getSystemService(VIBRATOR_SERVICE))).vibrate(millis);
        }
    }

    @Override
    public void run() {

        startTime = System.currentTimeMillis();

        while (isPlaying) {

            update();
            draw();
            sleep();
            fps();
            nbTours++;

        }

    }

    private void fps() {
        elapsedTime = System.currentTimeMillis() - startTime;
        if(elapsedTime >= 1000) {
            averageUPS = updateCount / (1E-3 * elapsedTime);
            averageFPS = frameCount / (1E-3 * elapsedTime);
            updateCount = 0;
            frameCount = 0;
            startTime = System.currentTimeMillis();
        }
    }

    private void update() {

        background1.update();
        background2.update();

        if (isGyro){
            vaisseau.toShoot = 1;
            vaisseau.x -= orientationData.getAccelOutput()[0] * Math.abs(orientationData.getAccelOutput()[0]);
            vaisseau.y += orientationData.getAccelOutput()[1] * Math.abs(orientationData.getAccelOutput()[1]);
            vaisseau.updateGyro();
        }
        else {
            vaisseau.update(playerPoint);
        }
        flammes.update(vaisseau);

        updateBullet();

        if (!bossWave) {
            updateEnnemi();
            updateExplosion();
            updateEnnemiBullet();
        }

        updateBoss();
        updateBulletBoss();

        trash.clear();
        trashEnemis.clear();
        updateCount++;
    }

    private void updateBoss() {

        if (bosses.size() == 0 && bossWave) {
            isGameWin = true;
            bossWave = false;
            bulletsBoss.clear();
            return;
        }

        List<Boss> trashBoss = new ArrayList<>();

        for (Boss boss : bosses) {

            boss.update();
            if (nbTours % 30 == 0) {
                bossShot(boss);
            }
            if (boss.getHp() <= 0) {
                trashBoss.add(boss);
            }
        }
        for(Boss boss : trashBoss) {
            bosses.remove(boss);
        }

    }

    public void bossShot(Boss boss) {
        Laser bullet1 = new Laser(getResources());
        Laser bullet2 = new Laser(getResources());
        bullet1.x =  (boss.x + (boss.width / 2)) - (bullet1.width / 2) - 50;
        bullet2.x =  (boss.x + (boss.width / 2)) - (bullet2.width / 2) + 50;
        bullet1.y = boss.y + (boss.height) - 50;
        bullet2.y = boss.y + (boss.height) - 50;
        bulletsBoss.add(bullet1);
        bulletsBoss.add(bullet2);
    }

    private void updateEnnemi() {

        if (ennemis.size() == 0) {
            isGameWin = true;
            return;
        }

        for (Ennemi ennemi : ennemis) {

            ennemi.update();
            if (ennemi.y > screenY) {
                trashEnemis.add(ennemi);
            }
            if (Rect.intersects(ennemi.getCollisionShape(), vaisseau.getCollisionShape())){
                isGameOver = true;
                return;
            }
            shoot(ennemi);
        }

        for (Ennemi ennemi : trashEnemis) {
            ennemis.remove(ennemi);
        }

    }

    private void shoot(Ennemi ennemi) {

        if (random.nextInt(500) == 0) {

            Laser bullet = new Laser(getResources());
            bullet.x =  (ennemi.x + (ennemi.width / 2)) - (bullet.width / 2);
            bullet.y = ennemi.y + (ennemi.height);
            bullet.speed = ennemi.speed + 20;
            bulletEnnemis.add(bullet);

        }

    }

    private void updateBulletBoss() {

        for (Laser laser : bulletsBoss) {

            if (laser.y > screenY) {
                trash.add(laser);
            }

            if (Rect.intersects(vaisseau.getCollisionShape(), laser.getCollisionShape())){
                isGameOver = true;
                vaisseau.isDead = true;
                return;
            }

            laser.y += 20 * screenRatioY;
        }

        for (Laser laser : trash) {
            bulletsBoss.remove(laser);
        }

    }

    private void updateEnnemiBullet() {

        for (Laser laser : bulletEnnemis) {

            if (laser.y > screenY) {
                trash.add(laser);
            }

            if (Rect.intersects(vaisseau.getCollisionShape(), laser.getCollisionShape())){
                isGameOver = true;
                vaisseau.isDead = true;
                return;
            }

            laser.y += laser.speed * screenRatioY;
        }

        for (Laser laser : trash) {
            bulletEnnemis.remove(laser);
        }

    }

    private void updateBullet() {

        for (Laser laser : vaisseau.getBullets()) {

            if (laser.y > screenY) {
                trash.add(laser);
            }

            for (Ennemi ennemi : ennemis) {

                if (Rect.intersects(ennemi.getCollisionShape(), laser.getCollisionShape())){

                    scoreWave.incr();
                    newExplosion(ennemi);
                    laser.y = screenY + 500;
                    trashEnemis.add(ennemi);

                }

            }

            for (Boss boss : bosses) {
                if (Rect.intersects(boss.getCollisionShape(), laser.getCollisionShape())){
                    trash.add(laser);
                    boss.setHp(boss.getHp()-1);
                    boss.isHurt = true;
                }
            }

            laser.y -= 30 * screenRatioY;
        }

        for (Laser laser : trash) {
            vaisseau.getBullets().remove(laser);
        }

    }

    private void updateExplosion() {
        List<Explosion> trash = new ArrayList<>();
        List<Nuclear> trashN = new ArrayList<>();

        for(Explosion explosion : explosions) {
            if (explosion.fini){
                trash.add(explosion);
            }
        }

        for (Explosion explosion : trash){
            explosions.remove(explosion);
        }

        for(Nuclear nuclear : nuclears) {
            nuclear.update();
        }
        for(Nuclear nuclear : trashN) {
            nuclears.remove(nuclear);
        }
    }

    private void draw() {

        if (getHolder().getSurface().isValid()) {

            Canvas canvas = getHolder().lockCanvas();

            canvas.drawBitmap(background1.background1, background1.x, background1.y, paint);
            canvas.drawBitmap(background2.background2, background2.x, background2.y, paint);

            if (isGameWin) {
                drawEnd(canvas);
                isGameWin = false;
                return;
            }

            canvas.drawBitmap(vaisseau.getFlight(), vaisseau.x, vaisseau.y, paint);
            canvas.drawBitmap(vaisseau.getFlight(), vaisseau.x, vaisseau.y, paint);
            flammes.draw(canvas);

            for (Laser laser : vaisseau.getBullets()) {
                canvas.drawBitmap(laser.laser, laser.x, laser.y, paint);
            }

            if (!bossWave) {

                for (Laser laser : bulletEnnemis) {
                    canvas.drawBitmap(laser.laserEnnemi, laser.x, laser.y, paint);
                }
                for (Nuclear nuclear : nuclears) {
                    nuclear.draw(canvas);
                }
                for (Ennemi ennemi : ennemis) {
                    canvas.drawBitmap(ennemi.getMechant(), ennemi.x, ennemi.y, paint);
                }

//                for (Explosion explosion : explosions) {
//                    canvas.drawBitmap(explosion.getExplosion(), explosion.x, explosion.y, paint);
//                }


                String textEnnemis = "Ennemies left ";
                canvas.drawText(textEnnemis + ennemis.size(), screenX - textEnnemis.length() * 30, 125, paintEnnemis);

            }

            for (Boss boss : bosses) {
                canvas.drawBitmap(boss.getBoss(), boss.x, boss.y, paint);
                canvas.drawText("HP: " + boss.getHp(), (float) (boss.x + boss.width*0.35), boss.y, paintBoss);
            }

            for (Laser laser : bulletsBoss) {
                canvas.drawBitmap(laser.laserEnnemi, laser.x, laser.y, paint);
            }

            if (isGameOver) {
                isPlaying = false;
                vibrate(500);
                saveScore();
                saveScoreIfBest();
                getHolder().unlockCanvasAndPost(canvas);
                AnimBitmap animBitmap = new AnimBitmap(this, canvas, getResources(), "vaisseau");
                animBitmap.lancer(background1, background2);
                endgame();
                return;
            }

            drawCenter(canvas, paint, scoreWave.getScore()+"", 164);
            canvas.drawText("Wave " + scoreWave.getWave(), 50, 125, paintWave);

            // affiche les fps
            Paint paintfps = new Paint();
            int color = ContextCompat.getColor(getContext(), R.color.magenta);
            paintfps.setColor(color);
            paintfps.setTextSize(50);
//            canvas.drawText("UPS: " + averageUPS, 100, 1000, paintfps);
//            canvas.drawText("FPS: " + averageFPS, 100, 1100, paintfps);

            if (isGyro) {
                canvas.drawText("accel x: " + orientationData.getAccelOutput()[0], 100, 300, paintfps);
                canvas.drawText("accel y: " + orientationData.getAccelOutput()[1], 100, 400, paintfps);
            }

            getHolder().unlockCanvasAndPost(canvas);
            frameCount++;
        }

    }

    private void saveScoreIfBest() {
        if (listeScores.size() < 10) {
            listeScores.add(scoreWave);
            Collections.sort(listeScores);
        }
        else {
            if (scoreWave.getScore() > listeScores.get(0).getScore()) {
                insertScore();
            }
        }
        SharedPreferences.Editor editor  = prefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(listeScores);
        editor.putString("listewavescores", json);
        editor.apply();
    }

    private void insertScore() {
        int i = listeScores.size()-1;
        while (i >= 0 && listeScores.get(i).compareTo(scoreWave) >= 0) {
            i--;
        }
        listeScores.remove(0);
        listeScores.add(i, scoreWave);
    }

    private void saveScore() {

        SharedPreferences.Editor editor  = prefs.edit();
        editor.putInt("nbwave", scoreWave.getWave());
        editor.putInt("lastscorewave", scoreWave.getScore());
        editor.apply();
    }

    private void endgame() {

        try {
            Thread.sleep(100);
            activity.startActivity(new Intent(activity, PostWaveActivity.class));
            activity.finish();

        } catch (InterruptedException e) {
            e.printStackTrace();
        }


    }

    public void drawEnd(Canvas canvas) {

        getHolder().unlockCanvasAndPost(canvas);
        resetAffichage();
        scoreWave.incrWave();
        Animation animation;
        if (scoreWave.getWave() % 2 == 0) animation = new Animation(canvas, "Boss Wave");
        else animation = new Animation(canvas, "Wave " + scoreWave.getWave() + "/10");
        animation.lancer(this, background1, background2);
        next(); // appel la nouvelle vague

    }

    public void resetAffichage() {

        vaisseau.toShoot=0;
        vaisseau.getBullets().clear();
        explosions.clear();
        nuclears.clear();
        bulletEnnemis.clear();
        ennemis.clear();
        playerPoint.set(screenX/2, (int) (1600 * screenRatioY));

    }

    public void next() {
        if (scoreWave.getWave() % 2 == 0) {
            bossWave();
        }
        else {
            nextWave();
        }
    }

    private void newEnnemi(int ligne, int col) {

        Ennemi ennemi = new Ennemi(getResources());
        ennemi.x = ligne * ennemi.width + 20;
        ennemi.y = 50 + (-ennemi.height * col);
        ennemis.add(ennemi);

    }

    private void firstWave() {

        for (int col = 0; col < 3; col++) {
            for (int ligne = 0; ligne<4; ligne++) {
                newEnnemi(ligne, col);
            }
        }

    }

    public void nextWave() {

        for (int i = 1; i<21; i++) {

            Ennemi ennemi = new Ennemi(getResources());
            ennemi.x = random.nextInt(screenX - ennemi.width);
            ennemi.y = -ennemi.height * i;
            ennemi.speed += scoreWave.getWave() * 5;
            ennemis.add(ennemi);
        }
    }

    private void bossWave() {

        Boss boss = new Boss(getResources());
        boss.x = (screenX- boss.width)/2 ;
        boss.y = (int) ((screenY /2) - boss.height*1.5);
        bosses.add(boss);
        bossWave = true;

    }

    public void sleep() {

        // Pause game loop to not exceed target UPS
        elapsedTime = System.currentTimeMillis() - startTime;
        sleepTime = (long) (updateCount*UPS_PERIOD - elapsedTime);
        if(sleepTime > 0) {
            try {
                Thread.sleep(sleepTime);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }

    public void resume() {
        isPlaying = true;
        thread = new Thread(this);
        thread.start();

    }

    public void pause() {

        try {
            isPlaying = false;
            thread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_MOVE:
                if (vaisseau.rectangle.contains((int) event.getX(), (int) event.getY()) && !isGameWin) {
                    playerPoint.set((int) event.getX(), (int) event.getY());
                    vaisseau.toShoot++;
                    performClick();
                }
        }
        return true;
    }
    @Override
    public boolean performClick() {
        // Calls the super implementation, which generates an AccessibilityEvent
        // and calls the onClick() listener on the view, if any
        super.performClick();
        // Handle the action for the custom click here
        return true;
    }

    private void newExplosion(Ennemi ennemi) {

        if (!prefs.getBoolean("isMute", false))
            soundPool.play(soundExplosion, 1, 1, 0, 0, 1);
        Explosion explosion = new Explosion(getResources());
        explosion.x = (ennemi.x + (ennemi.width / 2)) - explosion.width/2;
        explosion.y = (ennemi.y + ennemi.height) - explosion.height/2;
        explosions.add(explosion);
        nuclears.add(new Nuclear(explosion.x, explosion.y, getResources()));
    }

    public void newBullet() {

        if (!prefs.getBoolean("isMute", false))
            soundPool.play(soundBullet, 1, 1, 0, 0, 1);
    }

    private void drawCenter(Canvas canvas, Paint paint, String text, float y) {
        Rect r = new Rect();
        canvas.getClipBounds(r);
        int cHeight = r.height();
        int cWidth = r.width();
        paint.setTextAlign(Paint.Align.LEFT);
        paint.getTextBounds(text, 0, text.length(), r);
        float x = cWidth / 2f - r.width() / 2f - r.left;
//        float y = cHeight / 2f + r.height() / 2f - r.bottom;
        canvas.drawText(text, x, y, paint);
    }

    public void initPaint(){

        paint = new Paint();
        paint.setColor(Color.WHITE);
        paint.setTextSize(128);

        paintWave = new Paint();
        paintWave.setColor(Color.WHITE);
        paintWave.setTextSize(64);

        paintEnnemis = new Paint();
        paintEnnemis.setColor(Color.WHITE);
        paintEnnemis.setTextSize(50);

        paintBoss = new Paint();
        paintBoss.setColor(Color.WHITE);
        paintBoss.setTextSize(40);

        paintTransi = new Paint();
        paintTransi.setColor(Color.WHITE);
        paintTransi.setTextSize(115);

    }

    private void arrayInit() {
        bosses = new ArrayList<>();
        bulletsBoss = new ArrayList<>();

        ennemis = new ArrayList<>();
        trashEnemis = new ArrayList<>();
        bulletEnnemis = new ArrayList<>();

        explosions = new ArrayList<>();
        nuclears = new ArrayList<>();

        trash = new ArrayList<>(); // laser

        Gson gson = new Gson();
        String json = prefs.getString("listewavescores", null);
        Type type = new TypeToken<ArrayList<ScoreWave>>() {}.getType();
        listeScores = gson.fromJson(json, type);

        if (listeScores == null) {
            listeScores = new ArrayList<>();
        }
    }

    public VaisseauWave getVaisseau() {
        return vaisseau;
    }
}
