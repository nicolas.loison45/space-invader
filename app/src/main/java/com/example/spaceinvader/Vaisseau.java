package com.example.spaceinvader;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.graphics.Rect;

import static com.example.spaceinvader.GameView.nbTours;
import static com.example.spaceinvader.GameView.screenRatioX;
import static com.example.spaceinvader.GameView.screenRatioY;
import static com.example.spaceinvader.GameView.screenX;
import static com.example.spaceinvader.GameView.screenY;

public class Vaisseau {

    boolean isShielded = false, isVulnerable = true;
    Rect rectangle;
    int toShoot=0;
    int x, y, width, height, counter = 0, shootSpeed = 8;
    Bitmap vaisseau, dead, shield;
    private GameView gameView;
    private int shieldCounter;

    Vaisseau (GameView gameView, int screenX, Resources res) {

        this.gameView = gameView;

        vaisseau = BitmapFactory.decodeResource(res, R.drawable.vaisseau2);
        dead = BitmapFactory.decodeResource(res, R.drawable.mechant);
        shield = BitmapFactory.decodeResource(res, R.drawable.vaisseaushield);

        width = vaisseau.getWidth();
        height = vaisseau.getHeight();

        width = (int) (width * screenRatioX);
        height = (int) (height * screenRatioY);

        vaisseau = Bitmap.createScaledBitmap(vaisseau, width, height, false);
        dead = Bitmap.createScaledBitmap(dead, width, height, false);

        x = (screenX) / 2 ;
        y = (int) (1600 * screenRatioY);

        rectangle = new Rect(x, y, x + width, y + height);

    }

    Bitmap getFlight () {

        return vaisseau;
    }

    public void update(Point point) {

        if (toShoot != 0 && counter%shootSpeed == 0) { // tous les 8 appels, ça tire
            gameView.newBullet();
        }
        counter++;

        x = point.x - rectangle.width()/2;
        y = point.y - rectangle.height()/2;
        rectangle.set(x, y, point.x + rectangle.width()/2, point.y + rectangle.height()/2);

    }

    public void updateGyro() {
        if (toShoot != 0 && counter%shootSpeed == 0) { // tous les 8 appels, ça tire
            gameView.newBullet();
        }
        counter++;
        if (x + (width/2) > screenX) x = screenX - width/2;
        if (x + (width/2) < 0) x = -width/2;
        if ((y + height) > screenY) y = screenY - height;
        if ( y <= 0) y = 0;

        rectangle.set(x, y, x + width, y + height);
    }


    Rect getCollisionShape () {
        return rectangle;
    }

    public Bitmap getDead() {
        return dead;
    }

    private void normalState() {

        width = vaisseau.getWidth();
        height = vaisseau.getHeight();

        width = (int) (width * screenRatioX);
        height = (int) (height * screenRatioY);

        rectangle.set(new Rect(x, y, x + width, y + height));

    }

    public boolean isShield() {

        isShielded = true;
        shieldCounter=0;

        width = shield.getWidth();
        height = shield.getHeight();

        width = (int) (width * screenRatioX);
        height = (int) (height * screenRatioY);

        rectangle.set(new Rect(x, y, x + width, y + height));

        shield = Bitmap.createScaledBitmap(shield, width, height, false);


        return isShielded;
    }

    public Bitmap getShield() {

        shieldCounter++;
        if (shieldCounter == 150) {
            isShielded = false;
            normalState();
            gameView.shieldDown();
            shieldCounter = 0;
        }
        return shield;

    }

}
