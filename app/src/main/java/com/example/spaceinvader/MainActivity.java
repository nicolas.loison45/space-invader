package com.example.spaceinvader;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.google.firebase.analytics.FirebaseAnalytics;

import java.util.Objects;

import io.github.tonnyl.whatsnew.WhatsNew;
import io.github.tonnyl.whatsnew.item.WhatsNewItem;
import io.github.tonnyl.whatsnew.util.PresentationOption;


public class MainActivity extends AppCompatActivity {

    private FirebaseAnalytics mFirebaseAnalytics;
    private boolean isMute, isAccelerometer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Obtain the FirebaseAnalytics instance.
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_main);

        findViewById(R.id.play).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vibrate(500);
                startActivity(new Intent(MainActivity.this, GameActivity.class));
            }
        });

        findViewById(R.id.wave).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vibrate(500);
                startActivity(new Intent(MainActivity.this, WaveActivity.class));
            }
        });

        final SharedPreferences prefs = getSharedPreferences("game", MODE_PRIVATE);

        isMute = prefs.getBoolean("isMute", false);
        final ImageView volumeCtrl = findViewById(R.id.volumeCtrl);
        getMuteIcon(volumeCtrl, prefs);

        isAccelerometer = prefs.getBoolean("isGyro", true);
        final ImageView gyro = findViewById(R.id.accelerometer);
        getGyroIcon(gyro, prefs);

        TextView highscores = findViewById(R.id.highscores);
        highscores.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, HighscoresActivity.class));
            }
        });

        analytics();
    }


    public void vibrate(long millis) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            ((Vibrator) Objects.requireNonNull(getSystemService(VIBRATOR_SERVICE)))
                    .vibrate(VibrationEffect.createOneShot(millis, VibrationEffect.DEFAULT_AMPLITUDE));
        }
        else {
            ((Vibrator) Objects.requireNonNull(getSystemService(VIBRATOR_SERVICE))).vibrate(millis);
        }
    }

    public void getMuteIcon(final ImageView volumeCtrl, final SharedPreferences prefs) {

        if (isMute)
            volumeCtrl.setImageResource(R.drawable.ic_volume_off_white_24dp);
        else
            volumeCtrl.setImageResource(R.drawable.ic_volume_up_white_24dp);

        volumeCtrl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                isMute = !isMute;
                if (isMute)
                    volumeCtrl.setImageResource(R.drawable.ic_volume_off_white_24dp);
                else
                    volumeCtrl.setImageResource(R.drawable.ic_volume_up_white_24dp);

                SharedPreferences.Editor editor = prefs.edit();
                editor.putBoolean("isMute", isMute);
                editor.apply();

            }
        });
    }

    private void getGyroIcon(final ImageView gyro, final SharedPreferences prefs) {

        if (isAccelerometer)
            gyro.setImageResource(R.drawable.gyrowhiteon);
        else
            gyro.setImageResource(R.drawable.gyrowhiteoff);

        gyro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                isAccelerometer = !isAccelerometer;
                if (isAccelerometer)
                    gyro.setImageResource(R.drawable.gyrowhiteon);
                else
                    gyro.setImageResource(R.drawable.gyrowhiteoff);

                SharedPreferences.Editor editor = prefs.edit();
                editor.putBoolean("isGyro", isAccelerometer);
                editor.apply();

            }
        });
    }


    private void analytics() {
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "New arcade activity");
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, "Arcade");
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, "p7655");
        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);

        Bundle bundleWave = new Bundle();
        bundleWave.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "New wave activity");
        bundleWave.putString(FirebaseAnalytics.Param.ITEM_NAME, "Wave attack");
        bundleWave.putString(FirebaseAnalytics.Param.ITEM_ID, "p7654");
        mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundleWave);
    }
}