package com.example.spaceinvader;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.graphics.Rect;

import java.util.ArrayList;
import java.util.List;

import static com.example.spaceinvader.WaveView.nbTours;
import static com.example.spaceinvader.WaveView.screenRatioX;
import static com.example.spaceinvader.WaveView.screenRatioY;
import static com.example.spaceinvader.WaveView.screenX;
import static com.example.spaceinvader.WaveView.screenY;

public class VaisseauWave {

    public boolean isDead = false;
    boolean isShielded = false;
    Rect rectangle;
    int toShoot=0;
    int x, y, width, height, counter = 0, shootSpeed = 8;
    Bitmap vaisseau, shield, dead;
    private WaveView waveView;
    private List<Laser> bullets;
    private int shieldCounter;
    private Resources resources;

    VaisseauWave (WaveView waveView, int screenX, Resources res) {

        this.waveView = waveView;
        this.resources = res;

        vaisseau = BitmapFactory.decodeResource(res, R.drawable.vaisseau2);
        shield = BitmapFactory.decodeResource(res, R.drawable.vaisseaushield);

        width = vaisseau.getWidth();
        height = vaisseau.getHeight();

        vaisseau = Bitmap.createScaledBitmap(vaisseau, width, height, false);

        x = (screenX) / 2 ;
        y = (int) (1600 * screenRatioY);

        rectangle = new Rect(x, y, x + width, y + height);

        bullets = new ArrayList<>();

    }

    Bitmap getFlight () {
        return vaisseau;
    }

    public void update(Point point) {

        if (toShoot != 0 && counter%shootSpeed == 0) { // tous les 8 appels, ça tire
            newBullet();
        }
        counter++;

        rectangle.set(point.x - rectangle.width()/2, point.y - rectangle.height()/2, point.x + rectangle.width()/2, point.y + rectangle.height()/2);
        x = point.x - rectangle.width()/2;
        y = point.y - rectangle.height()/2;

    }

    public void updateGyro() {
        if (toShoot != 0 && counter%shootSpeed == 0) { // tous les 8 appels, ça tire
            newBullet();
        }
        counter++;

        if (x + (width/2) > screenX) x = screenX - width/2;
        if (x + (width/2) < 0) x = -width/2;
        if ((y + height) > screenY) y = screenY - height;
        if ( y <= 0) y = 0;

        rectangle.set(x, y, x + width, y + height);
    }

    Rect getCollisionShape () {
        return rectangle;
    }

    private void normalState() {

        width = vaisseau.getWidth();
        height = vaisseau.getHeight();

        width = (int) (width * screenRatioX);
        height = (int) (height * screenRatioY);

        rectangle.set(new Rect(x, y, x + width, y + height));

    }

    public boolean isShield() {

        isShielded = true;
        shieldCounter=0;

        width = shield.getWidth();
        height = shield.getHeight();

        width = (int) (width * screenRatioX);
        height = (int) (height * screenRatioY);

        rectangle.set(new Rect(x, y, x + width, y + height));

        shield = Bitmap.createScaledBitmap(shield, width, height, false);


        return isShielded;
    }


    public void newBullet() {

        Laser bullet = new Laser(this.resources);
        bullet.x =  (x + (width / 2)) - (bullet.width / 2);
        bullet.y = y - (bullet.height);
        bullets.add(bullet);
        waveView.newBullet();

    }

    public List<Laser> getBullets() {
        return this.bullets;
    }

}
