package com.example.spaceinvader;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;

import static com.example.spaceinvader.GameView.nbTours;
import static com.example.spaceinvader.GameView.screenRatioX;
import static com.example.spaceinvader.GameView.screenRatioY;
import static com.example.spaceinvader.GameView.screenY;

public class Mechant {

    Rect rectangle;
    public int speed;
    public boolean wasShot = true;
    int x, y = 0, width, height;
    Bitmap mechant;

    Mechant (Resources res) {

        mechant = BitmapFactory.decodeResource(res, R.drawable.mechant3);

        speed = 15;

        width = mechant.getWidth();
        height = mechant.getHeight();

        width = (int) (width * screenRatioX);
        height = (int) (height * screenRatioY);

        mechant = Bitmap.createScaledBitmap(mechant, width, height, false);

        x -= width;
        y = screenY + 20;

        rectangle = new Rect(x, y, x + width, y + height);


    }

    public void update(int random) {

        if (nbTours % 200 == 0) {
            speed += 5;
        }

        y += speed;
        rectangle.set(x, y, x + width, y + height);

        if (y > screenY) {isOut(random);}

    }

    public void isOut(int newX){

        y = -height*2;
        x = newX;
        wasShot = false;

    }

    Bitmap getMechant() {

        return mechant;

    }

    Rect getCollisionShape () {
        return rectangle;
    }

}