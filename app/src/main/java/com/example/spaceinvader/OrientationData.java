package com.example.spaceinvader;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

public class OrientationData implements SensorEventListener {

    private SensorManager manager;
    private Sensor accelerometer;

    private Context context;

    private float[] accelOutput = new float[3];

    public float[] getAccelOutput() {
        return accelOutput;
    }

    public OrientationData(Context context) {
        this.context = context;
        manager = (SensorManager)this.context.getSystemService(Context.SENSOR_SERVICE);
        assert manager != null;
        accelerometer = manager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
    }

    public void register() {
        manager.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_GAME);
    }

    public void pause() {
        manager.unregisterListener(this);
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if(event.sensor.getType() == Sensor.TYPE_ACCELEROMETER)
            accelOutput = event.values;
    }
}