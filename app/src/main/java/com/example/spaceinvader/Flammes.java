package com.example.spaceinvader;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;


public class Flammes {

    public int r, x, x2, y, width, height, numFrames;
    private Bitmap flammes;
    private Animatio animation = new Animatio();

    public Flammes(int x, int y, Resources res)
    {
        this.x = x;
        this.x2 = x;
        this.y = y;

        numFrames = 3;

        Bitmap[] image = new Bitmap[numFrames];
        flammes = BitmapFactory.decodeResource(res, R.drawable.flammesbleues);

        width = flammes.getWidth() / numFrames;
        height = flammes.getHeight();

        for (int i = 0; i < image.length; i++)
        {
            image[i] = Bitmap.createBitmap(flammes, i*width, 0, width, height);
        }

        animation.setFrames(image);
        animation.setDelay(50);

    }
    public void update(VaisseauWave vaisseau) {
        x = (vaisseau.x + (vaisseau.width / 4)) - (animation.getImage().getWidth() / 2);
        x2 = (vaisseau.x + (3*vaisseau.width / 4)) - (animation.getImage().getWidth() / 2);
        y = vaisseau.y + vaisseau.height - 30;

        animation.update();
    }

    public void draw(Canvas canvas)
    {
        canvas.drawBitmap(animation.getImage(),x,y,null);
        canvas.drawBitmap(animation.getImage(),x2,y,null);
    }

}