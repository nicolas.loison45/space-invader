package com.example.spaceinvader;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;


public class Explosion {

    public boolean fini = false;
    int x, y, width, height, explosionCounter = 0;
    Bitmap explosion1, explosion2;

    Explosion (Resources res) {

        explosion1 = BitmapFactory.decodeResource(res, R.drawable.explosion1);
        explosion2 = BitmapFactory.decodeResource(res, R.drawable.explosion2);

        width = explosion1.getWidth();
        height = explosion1.getHeight();

        width *= 2;
        height *= 2;

        explosion1 = Bitmap.createScaledBitmap(explosion1, width, height, false);
        explosion2 = Bitmap.createScaledBitmap(explosion2, width, height, false);


    }

    Bitmap getExplosion(){

        if (explosionCounter == 0){
            explosionCounter++;
            return explosion1;
        }
        fini = true;
        return explosion2;

    }

}
