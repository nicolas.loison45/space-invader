package com.example.spaceinvader;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

public class PostWaveActivity extends AppCompatActivity {

    private boolean isMute, isAccelerometer;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_post_wave);

        findViewById(R.id.retrywave).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(PostWaveActivity.this, WaveActivity.class));
            }
        });

        findViewById(R.id.home).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(PostWaveActivity.this, MainActivity.class));
            }
        });
        final SharedPreferences prefs = getSharedPreferences("game", MODE_PRIVATE);

        TextView scoreTxt = findViewById(R.id.scorewave);
        scoreTxt.setText(String.valueOf(scoreTxt.getText()) + prefs.getInt("lastscorewave", 0));

        TextView waveTxt = findViewById(R.id.wave);
        waveTxt.setText(String.valueOf(waveTxt.getText()) + prefs.getInt("nbwave", 0));
        waveTxt.setTextSize(45);

        isMute = prefs.getBoolean("isMute", false);

        final ImageView volumeCtrl = findViewById(R.id.volumeCtrl);

        if (isMute)
            volumeCtrl.setImageResource(R.drawable.ic_volume_off_white_24dp);
        else
            volumeCtrl.setImageResource(R.drawable.ic_volume_up_white_24dp);

        volumeCtrl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                isMute = !isMute;
                if (isMute)
                    volumeCtrl.setImageResource(R.drawable.ic_volume_off_white_24dp);
                else
                    volumeCtrl.setImageResource(R.drawable.ic_volume_up_white_24dp);

                SharedPreferences.Editor editor = prefs.edit();
                editor.putBoolean("isMute", isMute);
                editor.apply();

            }
        });

        isAccelerometer = prefs.getBoolean("isGyro", true);
        final ImageView gyro = findViewById(R.id.accelerometer);
        getGyroIcon(gyro, prefs);
    }

    private void getGyroIcon(final ImageView gyro, final SharedPreferences prefs) {

        if (isAccelerometer)
            gyro.setImageResource(R.drawable.gyrowhiteon);
        else
            gyro.setImageResource(R.drawable.gyrowhiteoff);

        gyro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                isAccelerometer = !isAccelerometer;
                if (isAccelerometer)
                    gyro.setImageResource(R.drawable.gyrowhiteon);
                else
                    gyro.setImageResource(R.drawable.gyrowhiteoff);

                SharedPreferences.Editor editor = prefs.edit();
                editor.putBoolean("isGyro", isAccelerometer);
                editor.apply();

            }
        });
    }
}