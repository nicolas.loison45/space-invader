package com.example.spaceinvader;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;

import static com.example.spaceinvader.WaveView.screenX;
import static com.example.spaceinvader.WaveView.screenY;

public class Animation {

    private Canvas canvas;
    private String text;
    private int xText;
    private Paint paint;
    private Rect rect;
    private boolean isPlaying = true;

    public Animation(Canvas canvas, String text) {
        this.canvas = canvas;
        this.text = text;
        this.paint = new Paint();
        paint.setColor(Color.WHITE);
        paint.setTextSize(115);
        this.rect = new Rect(0, screenY/2, screenX/2, (screenY+150)/2);
        xText = 65 * text.length();
    }

    public void lancer(WaveView waveView, Background background1, Background background2) {
        while(isPlaying) {
            update();
            draw(waveView, background1, background2);
            sleep();
        }
    }

    private void update() {
        rect.left += 5;
        if (rect.left+xText > screenX){
            isPlaying = false;
        }
    }

    private void draw(WaveView waveView, Background background1, Background background2) {
        if (waveView.getHolder().getSurface().isValid()) {
            Canvas canvas = waveView.getHolder().lockCanvas();

            canvas.drawBitmap(background1.background1, background1.x, background1.y, paint);
            canvas.drawBitmap(background2.background2, background2.x, background2.y, paint);
            this.canvas.drawText(this.text, rect.left, rect.bottom, this.paint);

            waveView.getHolder().unlockCanvasAndPost(canvas);
        }
    }

    private void sleep() {
        try {
            Thread.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public Canvas getCanvas() {
        return canvas;
    }

    public void setCanvas(Canvas canvas) {
        this.canvas = canvas;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Paint getPaint() {
        return paint;
    }

    public void setPaint(Paint paint) {
        this.paint = paint;
    }
}
