package com.example.spaceinvader;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;

import static com.example.spaceinvader.WaveView.screenX;
import static com.example.spaceinvader.WaveView.screenY;

public class AnimBitmap {

    private Canvas canvas;
    private Resources res;
    private Bitmap[] bitmaps;
    private Paint paint;
    private Rect rect;
    private boolean isPlaying = true;
    private int taille, i = 0;
    private Bitmap bitmapActuel;
    private VaisseauWave vaisseau;
    private WaveView waveView;

    public AnimBitmap(WaveView waveView, Canvas canvas, Resources resources, String text) {
        this.waveView = waveView;
        this.canvas = canvas;
        this.res = resources;
        this.paint = new Paint();
        this.vaisseau = waveView.getVaisseau();
        this.rect = new Rect();
        chooseAnim(text);
    }


    public void lancer(Background background1, Background background2) {
        while(isPlaying) {
            update();
            draw(background1, background2);
            sleep();
        }
    }

    private void update() {
        if (i >= taille){
            isPlaying = false;
            return;
        }
        bitmapActuel = bitmaps[i];
        i++;
    }

    private void draw(Background background1, Background background2) {
        if (waveView.getHolder().getSurface().isValid()) {
            Canvas canvas = waveView.getHolder().lockCanvas();

            canvas.drawBitmap(background1.background1, background1.x, background1.y, paint);
            canvas.drawBitmap(background2.background2, background2.x, background2.y, paint);

            this.canvas.drawBitmap(bitmapActuel, vaisseau.x, vaisseau.y, this.paint);

            waveView.getHolder().unlockCanvasAndPost(canvas);
        }
    }

    private void sleep() {
        try {
            Thread.sleep(100);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }



    private void chooseAnim(String text) {
        switch (text){
            case "vaisseau":
                vaisseauDeath();
                break;
            case "ennemi":
                ennemiDeath();
        }
    }

    private void vaisseauDeath() {
        this.taille = 5;
        bitmaps = new Bitmap[taille];
        Bitmap dead1 = BitmapFactory.decodeResource(res, R.drawable.dead);
        Bitmap dead2 = BitmapFactory.decodeResource(res, R.drawable.dead2);
        dead1 = Bitmap.createScaledBitmap(dead1, dead1.getWidth()*2, dead1.getHeight()*2, false);
        dead2 = Bitmap.createScaledBitmap(dead2, dead2.getWidth()*2, dead2.getHeight()*2, false);
        Bitmap damage3 = BitmapFactory.decodeResource(res, R.drawable.damage3);
        Bitmap damage2 = BitmapFactory.decodeResource(res, R.drawable.damage2);
        Bitmap damage1 = BitmapFactory.decodeResource(res, R.drawable.damage1);
        bitmaps[0] = dead1;
        bitmaps[1] = dead2;
        bitmaps[2] = damage3;
        bitmaps[3] = damage2;
        bitmaps[4] = damage1;
        this.rect.set(vaisseau.rectangle);
    }

    private void ennemiDeath() {
        this.taille = 1;
        bitmaps = new Bitmap[taille];
        Bitmap damage3 = BitmapFactory.decodeResource(res, R.drawable.explosion1);
        Bitmap damage2 = BitmapFactory.decodeResource(res, R.drawable.explosion2);
        bitmaps[0] = damage3;
        bitmaps[1] = damage2;
    }
}
