package com.example.spaceinvader;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Point;
import android.os.Bundle;
import android.view.WindowManager;

public class WaveActivity extends AppCompatActivity {


    private WaveView waveView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        Point point = new Point();
        getWindowManager().getDefaultDisplay().getSize(point);

        waveView = new WaveView(this, point.x, point.y);

        setContentView(waveView);
    }

    @Override
    protected void onPause() {
        super.onPause();
        waveView.pause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        waveView.resume();
    }
}