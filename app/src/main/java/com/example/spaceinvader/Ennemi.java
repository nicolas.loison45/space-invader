package com.example.spaceinvader;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static com.example.spaceinvader.WaveView.nbTours;
import static com.example.spaceinvader.WaveView.screenRatioX;
import static com.example.spaceinvader.WaveView.screenRatioY;
import static com.example.spaceinvader.WaveView.screenY;

public class Ennemi {

    Rect rectangle;
    int speed;
    public boolean wasShot = true;
    int x, y = 0, width, height;
    private Resources resources;
    Bitmap mechant;

    Ennemi (Resources res) {

        this.resources = res;

        mechant = BitmapFactory.decodeResource(res, R.drawable.enemyred);

        speed = 2;

        width = mechant.getWidth();
        height = mechant.getHeight();

        width = (int) (width * screenRatioX);
        height = (int) (height * screenRatioY);

        mechant = Bitmap.createScaledBitmap(mechant, width, height, false);

        x -= width;
        y = screenY + 20;

        rectangle = new Rect(x, y, x + width, y + height);

    }

    public void update() {

        if (nbTours % 200 == 0) {
            speed += 2;
        }

        y += speed;
        rectangle.set(x, y, x + width, y + height);

    }


    public Bitmap getMechant() { return mechant; }

    public Rect getCollisionShape () {
        return rectangle;
    }

}
