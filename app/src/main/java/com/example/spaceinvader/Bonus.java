package com.example.spaceinvader;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;

import static com.example.spaceinvader.GameView.screenRatioX;
import static com.example.spaceinvader.GameView.screenRatioY;

public class Bonus {

    int x, y, width, height;
    String powerup;
    Bitmap speedup, shield;

    Bonus (Resources res, String powerUp) {

        powerup = powerUp;

        speedup = BitmapFactory.decodeResource(res, R.drawable.speedup);
        shield = BitmapFactory.decodeResource(res, R.drawable.shield);

        width = speedup.getWidth();
        height = speedup.getHeight();

        width = (int) (width * screenRatioX);
        height = (int) (height * screenRatioY);

        speedup = Bitmap.createScaledBitmap(speedup, width, height, false);
        shield = Bitmap.createScaledBitmap(shield, width, height, false);

    }

    Rect getCollisionShape () {
        return new Rect(x, y, x + width, y + height);
    }

}
