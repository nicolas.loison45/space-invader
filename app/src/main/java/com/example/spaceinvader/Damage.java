package com.example.spaceinvader;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;

import static com.example.spaceinvader.GameView.screenRatioX;
import static com.example.spaceinvader.GameView.screenRatioY;

public class Damage {

    public boolean fini = false;
    int x, y, width, height, damageCounter = 0;
    Bitmap damage1, damage2, damage3;

    Damage (Resources res) {


        damage1 = BitmapFactory.decodeResource(res, R.drawable.damage1);
        damage2 = BitmapFactory.decodeResource(res, R.drawable.damage2);
        damage3 = BitmapFactory.decodeResource(res, R.drawable.damage3);

        width = damage1.getWidth();
        height = damage1.getHeight();

        width = (int) (width * screenRatioX);
        height = (int) (height * screenRatioY);

        damage1 = Bitmap.createScaledBitmap(damage1, width, height, false);
        damage2 = Bitmap.createScaledBitmap(damage2, width, height, false);
        damage3 = Bitmap.createScaledBitmap(damage3, width, height, false);

    }

    Bitmap getDamage() {

        if (damageCounter == 0){
            damageCounter++;
            return damage1;
        }
        if (damageCounter == 1){
            damageCounter++;
            return damage2;
        }
        fini = true;
        return damage3;

    }


}
